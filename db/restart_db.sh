#!/bin/bash
  
sudo docker container stop my-postgres
sudo docker container rm my-postgres
sudo docker run -d -p 5432:5432 -e POSTGRES_PASSWORD=1234 -e POSTGRES_USER=postgres -v psql_data:/var/lib/postgresql/data --name my-postgres postgres
