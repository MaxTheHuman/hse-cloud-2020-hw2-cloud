#!/bin/bash

sudo apt-get update
sudo apt-get -y install docker.io

sudo docker volume create --name psql_data
sleep 2s

sudo docker run -d -p 5432:5432 \
        -e POSTGRES_PASSWORD=1234 \
        -e POSTGRES_USER=postgres \
        -v psql_data:/var/lib/postgresql/data \
        --name my-postgres postgres
sleep 2s
echo "OK"

sudo docker exec -it my-postgres psql -U postgres -c "create database statuses"


