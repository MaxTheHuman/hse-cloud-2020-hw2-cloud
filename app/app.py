from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import requests

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:1234@db_ip:5432/postgres"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class ServiceStatus(db.Model):
    __tablename__ = 'service_status'

    unique_id = db.Column(db.Integer, primary_key = True)
    ip = db.Column(db.String())
    status = db.Column(db.String())

    def __init__(self, ip, status):
        # self.unique_id = unique_id
        self.ip = ip
        self.status = status

    def __repr__(self):
        return f"{self.unique_id}:{self.ip}:{self.status}"

try:
    db.create_all()
except:
    print("db unavailable at start")


@app.route('/healthcheck', methods = ['GET'])
def healthcheck():
    if request.method == 'GET':
        try:
            rows = db.session.query(ServiceStatus).all()
            available_services = []
            for row in rows:
                if row.status == 'AVAILABLE':
                    available_services.append({"ip": row.ip, "status": row.status})
            return jsonify({
                "ip": curr_ip,
                "services": available_services
            })
        except:
            return jsonify({"error": "Database is unavailable"})


if __name__ == '__main__':
    curr_ip = requests.get('http://169.254.169.254/latest/meta-data/local-ipv4').text
    curr_status = "AVAILABLE"
    try:
        old_row = db.session.query(ServiceStatus).filter_by(ip = curr_ip)
        if old_row.count() == 0:
            new_row = ServiceStatus(ip = curr_ip, status = curr_status)
            db.session.add(new_row)
        else:
            num_rows_updated = ServiceStatus.query.filter_by(ip = curr_ip).update(dict(status=curr_status))
        db.session.commit()
    except:
        print("exception caught")
    app.run(debug=False, host='0.0.0.0')
