#!/bin/bash
  
sudo docker container rm my-app
sudo docker build -t my-app-image .
sudo docker run -it --network=host -p 5000:5000 --name=my-app my-app-image
