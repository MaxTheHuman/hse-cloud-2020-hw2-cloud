#!/bin/bash
IFS=$'\n' ips=($(cat terraform_output.txt | tail -4 | sed -e 's/.*= "\(.*\)".*/\1/'))
nginx_ip=${ips[0]}
echo nginx_ip: $nginx_ip
db_ip=${ips[3]}
echo db_ip: $db_ip
ssh -o StrictHostKeyChecking=no -A -J ubuntu@$nginx_ip ubuntu@$db_ip 'bash -s' < setup_db.sh

