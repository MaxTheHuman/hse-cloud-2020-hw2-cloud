#!/bin/bash

yc compute instance remove-one-to-one-nat terraform-database --network-interface-index 0
yc compute instance remove-one-to-one-nat terraform-app1 --network-interface-index 0
yc compute instance remove-one-to-one-nat terraform-app2 --network-interface-index 0

