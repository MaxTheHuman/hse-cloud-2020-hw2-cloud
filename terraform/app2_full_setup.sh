#!/bin/bash
IFS=$'\n' ips=($(cat terraform_output.txt | tail -4 | sed -e 's/.*= "\(.*\)".*/\1/'))
nginx_ip=${ips[0]}
echo nginx_ip: $nginx_ip
app2_ip=${ips[2]}
echo app2_ip: $app2_ip
db_ip=${ips[3]}
echo db_ip: $db_ip

HOSTS_LINE="$db_ip db_ip"

ssh -o StrictHostKeyChecking=no -A -J ubuntu@$nginx_ip ubuntu@$app2_ip 'sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts" && exit'

ssh -o StrictHostKeyChecking=no -A -J ubuntu@$nginx_ip ubuntu@$app2_ip 'bash -s' < setup_app.sh

