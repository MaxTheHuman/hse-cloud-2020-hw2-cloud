terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
}

provider "yandex" {
  token     = "AgAAAAAErmawAATuwehTBQzo8UTWvGrpMfj0TJs"
  cloud_id  = "terraform-cloud"
  folder_id = "b1gtscbu0kbo73r8v4ju"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "vm-1" {
  name = "terraform-nginx"

  resources {
    cores  = 2
    core_fraction = 5
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd83bj827tp2slnpp7f0"
      size = 13
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "vm-2" {
  name = "terraform-database"

  resources {
    cores  = 2
    core_fraction = 5
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd83bj827tp2slnpp7f0"
      size = 13
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "vm-3" {
  name = "terraform-app1"

  resources {
    cores  = 2
    core_fraction = 5
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd83bj827tp2slnpp7f0"
      size = 13
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "vm-4" {
  name = "terraform-app2"

  resources {
    cores  = 2
    core_fraction = 5
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd83bj827tp2slnpp7f0"
      size = 13
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

output "external_ip_address_nginx" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "internal_ip_address_database" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}

output "internal_ip_address_app_1" {
  value = yandex_compute_instance.vm-3.network_interface.0.ip_address
}

output "internal_ip_address_app_2" {
  value = yandex_compute_instance.vm-4.network_interface.0.ip_address
}
