#!/bin/bash
IFS=$'\n' ips=($(cat terraform_output.txt | tail -4 | sed -e 's/.*= "\(.*\)".*/\1/'))
nginx_ip=${ips[0]}
echo nginx_ip: $nginx_ip
app1_ip=${ips[1]}
echo app1_ip: $app1_ip
db_ip=${ips[3]}
echo db_ip: $db_ip

HOSTS_LINE="$db_ip db_ip"

ssh -o StrictHostKeyChecking=no -A -J ubuntu@$nginx_ip ubuntu@$app1_ip 'sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts" && exit'

ssh -o StrictHostKeyChecking=no -A -J ubuntu@$nginx_ip ubuntu@$app1_ip 'bash -s' < setup_app.sh

