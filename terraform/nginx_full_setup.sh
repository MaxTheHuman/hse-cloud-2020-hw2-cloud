#!/bin/bash
IFS=$'\n' ips=($(cat terraform_output.txt | tail -4 | sed -e 's/.*= "\(.*\)".*/\1/'))
nginx_ip=${ips[0]}
echo nginx_ip: $nginx_ip
app1_ip=${ips[1]}
echo app1_ip: $app1_ip
app2_ip=${ips[2]}
echo app2_ip: $app2_ip

cat ../nginx/nginx.conf.template | sed -e "s/\(.*server \)\(app1_ip\)\(.*\)/\1$app1_ip\3/" > \
	../nginx/nginx.conf_helper
cat ../nginx/nginx.conf_helper | sed -e "s/\(.*server \)\(app2_ip\)\(.*\)/\1$app2_ip\3/" > \
        ../nginx/nginx.conf



docker build -t maxsev1999/cloud-nginx:1.0.0 ../nginx
docker push maxsev1999/cloud-nginx:1.0.0

sleep 2s

ssh -o StrictHostKeyChecking=no -A ubuntu@$nginx_ip 'bash -s' < setup_nginx.sh

echo nginx_ip: $nginx_ip

